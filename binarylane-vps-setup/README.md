### Example Command:   
~~~
ansible-playbook -i 103.230.157.84 \
binarylane-vps-setup.yaml \
-e user_name=bob \
-e ssh_key_url=https://gitlab.com/username.keys \
-e ssh_port=6969 \
-e password_hash='blah' \
-e server=google.com \
--ask-pass
~~~
