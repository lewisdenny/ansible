First install Ansible and the Podman collection:
```bash
dnf install ansible
ansible-galaxy collection install containers.podman
```

Then run the following command replacing $user with a user on yout local system: `ansible-playbook deepstack.yml -e user=$username -K`

Sudo is required to ensure podman is installed and open the port in the firewall, the root user is not used for the container at all.

Once the container is running you can use `python deepstack-test.py` to test the API with the provided image.
