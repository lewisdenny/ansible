### Usage:
Source the overcloudrc file:
```bash
source overcloudrc
```
Download the playbook you would like:
```bash
curl https://gitlab.com/lewisdenny/ansible/-/raw/master/deploying_openstack_instances/provider-network-instance.yaml -O
```

Edit the network values for your network configuration if you like then run it:
```bash 
ansible-playbook provider-network-instance.yaml
```
